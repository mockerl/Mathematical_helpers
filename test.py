import logging
import re
import socket
import telnetlib
import threading
from collections import namedtuple
from datetime import timedelta

import voluptuous as vol

from homeassistant.components.device_tracker import (DOMAIN, PLATFORM_SCHEMA)
from homeassistant.const import (
    CONF_HOST, CONF_PASSWORD, CONF_USERNAME, CONF_PORT)
from homeassistant.util import Throttle
import homeassistant.helpers.config_validation as cv

_LEASES_CMD = 'cat /var/lib/misc/dnsmasq.leases'
_LEASES_REGEX = re.compile(
    r'\w+\s' +
    r'(?P<mac>(([0-9a-f]{2}[:-]){5}([0-9a-f]{2})))\s' +
    r'(?P<ip>([0-9]{1,3}[\.]){3}[0-9]{1,3})\s' +
    r'(?P<host>([^\s]+))')

# Command to get both 5GHz and 2.4GHz clients
_WL_CMD = '{ wl -i eth2 assoclist & wl -i eth1 assoclist ; }'
_WL_REGEX = re.compile(
    r'\w+\s' +
    r'(?P<mac>(([0-9A-F]{2}[:-]){5}([0-9A-F]{2})))')

_ARP_CMD = 'arp -n'
_ARP_REGEX = re.compile(
    r'.+\s' +
    r'\((?P<ip>([0-9]{1,3}[\.]){3}[0-9]{1,3})\)\s' +
    r'.+\s' +
    r'(?P<mac>(([0-9a-f]{2}[:-]){5}([0-9a-f]{2})))' +
    r'\s' +
    r'.*')

_IP_NEIGH_CMD = 'ip neigh'
_IP_NEIGH_REGEX = re.compile(
    r'(?P<ip>([0-9]{1,3}[\.]){3}[0-9]{1,3}|'
    r'([0-9a-fA-F]{1,4}:){1,7}[0-9a-fA-F]{0,4}(:[0-9a-fA-F]{1,4}){1,7})\s'
    r'\w+\s'
    r'\w+\s'
    r'(\w+\s(?P<mac>(([0-9a-f]{2}[:-]){5}([0-9a-f]{2}))))?\s'
    r'\s?(router)?'
    r'(?P<status>(\w+))')

_NVRAM_CMD = 'nvram get client_info_tmp'
_NVRAM_REGEX = re.compile(
    r'.*>.*>' +
    r'(?P<ip>([0-9]{1,3}[\.]){3}[0-9]{1,3})' +
    r'>' +
    r'(?P<mac>(([0-9a-fA-F]{2}[:-]){5}([0-9a-fA-F]{2})))' +
    r'>' +
    r'.*')

AsusWrtResult = namedtuple('AsusWrtResult', 'neighbors leases arp nvram')
_LOGGER = logging.getLogger(__name__)


class _Connection:
    def __init__(self):
        self._connected = False

    @property
    def connected(self):
        """Return connection state."""
        return self._connected

    def connect(self):
        """Mark currenct connection state as connected."""
        self._connected = True

    def disconnect(self):
        """Mark current connection state as disconnected."""
        self._connected = False

class TelnetConnection(_Connection):
    """Maintains a Telnet connection to an ASUS-WRT router."""

    def __init__(self, host, port, username, password, ap):
        """Initialize the Telnet connection properties."""
        super(TelnetConnection, self).__init__()

        self._telnet = None
        self._host = "192.168.1.1"
        self._port = 23
        self._username = "admin"
        self._password = "Amadeus1"
        self._ap = ap
        self._prompt_string = None

    def get_result(self):
        """Retrieve a single AsusWrtResult through a Telnet connection.
        Connect to the Telnet server if not currently connected, otherwise
        use the existing connection.
        """
        try:
            if not self.connected:
                self.connect()

            self._telnet.write('{}\n'.format(_IP_NEIGH_CMD).encode('ascii'))
            neighbors = (self._telnet.read_until(self._prompt_string).
                         split(b'\n')[1:-1])
            if self._ap:
                self._telnet.write('{}\n'.format(_ARP_CMD).encode('ascii'))
                arp_result = (self._telnet.read_until(self._prompt_string).
                              split(b'\n')[1:-1])
                self._telnet.write('{}\n'.format(_WL_CMD).encode('ascii'))
                leases_result = (self._telnet.read_until(self._prompt_string).
                                 split(b'\n')[1:-1])
                self._telnet.write('{}\n'.format(_NVRAM_CMD).encode('ascii'))
                nvram_result = (self._telnet.read_until(self._prompt_string).
                                split(b'\n')[1].split(b'<')[1:])
            else:
                arp_result = ['']
                nvram_result = ['']
                self._telnet.write('{}\n'.format(_LEASES_CMD).encode('ascii'))
                leases_result = (self._telnet.read_until(self._prompt_string).
                                 split(b'\n')[1:-1])
            return AsusWrtResult(neighbors, leases_result, arp_result,
                                 nvram_result)
        except EOFError:
            _LOGGER.error("Unexpected response from router")
            self.disconnect()
            return None
        except ConnectionRefusedError:
            _LOGGER.error("Connection refused by router. Telnet enabled?")
            self.disconnect()
            return None
        except socket.gaierror as exc:
            _LOGGER.error("Socket exception: %s", exc)
            self.disconnect()
            return None
        except OSError as exc:
            _LOGGER.error("OSError: %s", exc)
            self.disconnect()
            return None

    def connect(self):
        """Connect to the ASUS-WRT Telnet server."""
        self._telnet = telnetlib.Telnet(self._host)
        self._telnet.read_until(b'login: ')
        self._telnet.write((self._username + '\n').encode('ascii'))
        self._telnet.read_until(b'Password: ')
        self._telnet.write((self._password + '\n').encode('ascii'))
        self._prompt_string = self._telnet.read_until(b'#').split(b'\n')[-1]

        super(TelnetConnection, self).connect()

    def disconnect(self):   \
            # pylint: disable=broad-except
        """Disconnect the current Telnet connection."""
        try:
            self._telnet.write('exit\n'.encode('ascii'))
        except Exception:
            pass

        super(TelnetConnection, self).disconnect()


print("es geht los")

print(TelnetConnection(1,1,1,1,1).get_result().nvram)